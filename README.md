# Project Name

Todo List Application

## Description

This is a Vue 3 project with TypeScript, utilizing Bootstrap 5 and Pinia for state management. It is structured for optimal use with Vue and TypeScript.

## Installation

1. Make sure you have Node.js and npm installed on your system.
2. Clone the repository:

```bash
git clone https://gitlab.com/ainatojosoa/todolistproject.git
```

3. Navigate to the project directory:

```bash
cd todolistproject
```

4. Install the necessary dependencies:

```bash
npm install
```

## Usage

To run the project, use the following command:

```bash
npm run dev
```

## Technologies Used

- Vue 3
- TypeScript
- Bootstrap 5
- Pinia


## Author

- Aina Tojosoa ANDRIATSALAMA