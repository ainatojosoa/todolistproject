export interface TodoItem {
    todo: string;
    id: number;
    completed: boolean;
}