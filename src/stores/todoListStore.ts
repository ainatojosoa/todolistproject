import { defineStore } from "pinia";
import { type TodoItem } from "@/models/TodoItem";

export const todoListStore = defineStore('todoList', {
    state: () => ({
        todoList: [
            { todo: 'Default Todo already completed', id: 0, completed: true }
        ] as TodoItem[],
        id: 1
    }),
    actions: {
        addTodo(todo: string) {
            this.todoList.push({
                todo, id: this.id++, completed: false,
            })
        },
        deleteTodo(itemID: any) {
            this.todoList = this.todoList.filter((object) => {
                return object.id !== itemID
            })
        },
        todoCompleted(idToFind: number) {
            
            const todo = this.todoList.find((obj) => {
                obj.id === idToFind
            })
            if (todo) {
                todo.completed = !todo.completed;
            }
        },
        editTodo(id: number, newItem: string) {
            const todo = this.todoList.find((obj) => obj.id === id);
            if (todo) {
                todo.todo = newItem;
            }
        }
    }
})